terraform {
  backend "s3" {
    bucket         = "mak-terraform" # change this
    key            = "./terraform.tfstate"
    region         = "us-west-1"
    encrypt        = true
    dynamodb_table = "terraform-lock"
  }
}