variable "ami" {
  description = "value"
}

variable "instance_type" {
  description = "value"
  type = map(string)

  default = {
    "dev" = "t2.micro"
    "stage" = "t2.medium"
    "prod" = "t2.xlarge"
  }
}

variable "environment" {
  type    = string
  default = "stage"
}

module "ec2_instance" {
  source = "./modules_ec2_instance"
  ami = var.ami
  instance_type = lookup(var.instance_type, var.environment, "t2.micro")
}